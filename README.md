Changepoint Detection Analysis applied to CloudLab's Benchmarking Results
===========

This repository contains data and analysis code for CloudLab's changepoint detection analysis.

Data:

- `https://zenodo.org/record/3686952` -- Zenodo repository with all results (raw measurements and detected changepoints).

Code, published in the form of Google Colab notebooks:

- RAW (runs *first*): `https://colab.research.google.com/drive/18OsLOMb7oqgwGhGWPLDgN15mfxAz4sYm`
- AV, Analysis and Visualization (runs *second*): `https://colab.research.google.com/drive/1R8uSJ-kkQLn47sbvllUv3GMyz4B9tZnZ`

The last notebook "consumes" the results produced by the first one. To make running these notebooks more convenient and independent, we have uploaded the results produced by the first notebook into Zenodo (see `cpd_results.tar.gz` in the repository referenced above).

- CPD runtime analysis (runs independently from the rest of the notebooks): `https://colab.research.google.com/drive/1URUwMG4Y_Afpa1OxVGSsz3W_OfYhuERf`
- We have also made available a notebook with a simple working example of the `robseg`-based changepoint detection (for additional info, refer to: `https://arxiv.org/abs/1609.07363`) at: `https://colab.research.google.com/drive/14tfY6vozn0c3vyGy64I_zT4cbtMKaX3C`. This notebook runs independently from the rest of our notebooks.
- `db2csv.ipynb` notebook from this repositoty was used to extract all available measurements from the live database dedicated to this projects and save specific datasets into a collection of csv files, the ones that formed the raw portion of the aforementioned Zenodo repository. 

===========

With comments and questions related to this data and analysis, please contact: Dmitry Duplyakin (<dmdu@cs.utah.edu>)

===========

Contributors:
- Dmitry Duplyakin (<dmdu@cs.utah.edu>) 
- Alexandru Uta (<a.uta@vu.nl>)
- Aleksander Maricq (<amaricq@cs.utah.edu>)
- Robert Ricci (<ricci@cs.utah.edu>)
